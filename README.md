# Mavtek Chat Exercise

Hi and welcome to **Mavtek**'s Code Challenge. Please take your time to read the requirements carefully. You have four days to submit an answer in form of a development branch.

# Problem

Build *the backend* of a Chat Room application using Java 8 or Java 11. Your artifact must be either deployable (a WAR file) or executable (a JAR file). The goal is to allow a set of *fixed* users to exchange messages thorough a named boundary called "chat room". 

Imagine that your backend application will be used by a web client.

# Use cases

- There is an administrator that creates and destroys rooms.
- A user may *join* a room as long as it exists.
- A user may *leave* a room as long as it exists.
- A user may attempt to claim a nickname when he *joins* the chat room. If the nickname has already been claimed the system will assign a random nickname.
- A user may publish messages in the chat room they are in and these messages are to be seen by all other users in that room.
- A user may send messages to a specific user as long as they're both present in the room.

# Bonuses

- A live notification mechanism. 
- Unit tests.
- Integration tests.
- Any flavor of documentation.
- A clear Maven or a Gradle file that allows the server to start, run the unit/integration tests and depicts the internal hierchy of the project.

# To Clarify in your Pull Request
- What development framework and libraries did you choose and why ?
- What could be improved ?
